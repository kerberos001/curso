require('dotenv').config()


const config = {
    dev: process.env.NODE_ENV !== 'production',
    port: process.env.PORT || 3000,
    cors:process.env.CORS,
    db_user:process.env.DB_USER,
    db_password:process.env.DB_PASSWORD,
    db_host1:process.env.DB_HOST1,
    db_host2:process.env.DB_HOST2,
    db_host3:process.env.DB_HOST3,
    db_name:process.env.DB_NAME,
    db_port:process.env.DB_PORT

}

module.exports = { config }


