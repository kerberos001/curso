const {MongoClient, ObjectId} = require('mongodb')

const { config } = require('../config')

const USER = encodeURIComponent(config.db_user)
const PASSWORD = encodeURIComponent(config.db_password)
const DB_NAME = config.db_name

const MONGO_URI = `mongodb://${USER}:${PASSWORD}@${config.db_host1}:${config.db_port},${config.db_host2}:${config.db_port},${config.db_host2}:${config.db_port}/${DB_NAME}?replicaSet=Cluster0-shard-0&ssl=true&authSource=admin`
//'mongodb://admin711:ty0wPxq1anmhv81U@cluster0-shard-00-00-txclx.mongodb.net:27017,cluster0-shard-00-01-txclx.mongodb.net:27017,cluster0-shard-00-02-txclx.mongodb.net:27017/dbMovies?replicaSet=Cluster0-shard-0&ssl=true&authSource=admin'

console.log(MONGO_URI);

class MongoLib {

    constructor() {
        this.client = new MongoClient(MONGO_URI, { useNewUrlParser: true })
        this.dbName = DB_NAME
    }

    connect() {
        if(!MongoLib.connection) {
            MongoLib.connection = new Promise((resolve, reject ) => {
                this.client.connect(err => {
                    if(err) {
                        reject(err)
                    }
                    console.log('Connect succesFull')
                    resolve(this.client.db(this.dbName))
                })
            })
        }
        return MongoLib.connection
    }

    getAll(collection, query) {
        return this.connect().then(db => {
            return db.collection(collection).find(query).toArray()
        })
    }

    get(collection, id) {
        return this.connect().then(db => {
            return db.collection(collection).findOne({_id: ObjectId(id)})
        })
    }

    create(collection, data) {
        return this.connect().then(db => {
            return db.collection(collection).insertOne(data)
        }).then(result => result.insertId)
    }

    update(collection, id, data) {
        return this.connect().then(db => {
            return db.collection(collection).updateOne({_id: ObjectId(id)},{ $set: data }, { upsert:true })
        }).then(result => result.upsertedId || id)
    }

    delete(collection, id) {
        return this.connect().then(db => {
            return db.collection(collection).deleteOne({_id: ObjectId(id)})
        }).then(() => id)
    }


}

module.exports = MongoLib
