const express = require('express')
const MoviesService = require('../services/movies')
const { movieIdSchema,
    createMovieSchema,
    updateMovieShema } = require('../utils/schemas/movies')

const validationHandler = require('../utils/middleware/validationHandler')
const cacheResponse = require('../utils/cacheResponse')
const {FIVE_MINUTES_IN_SECONDS,
    SIXTY_MINUTES_IN_SECONDS} = require('../utils/time')

function moviesApi(app) {
    const router = express.Router()
    app.use('/api/movies', router)

    const moviesService = new MoviesService()

//GET all
    router.get('/', async (req, res, next)=> {
        cacheResponse(res, FIVE_MINUTES_IN_SECONDS)
        const { tags } = req.query    
        try {
            const movies = await moviesService.getMovies({ tags })

            res.status(200).json({
                data: movies,
                message: 'movies listed'
            }) 
                

        } catch (error) {
            next(error)
        }
    })
    //GET id
    router.get('/:movieId', validationHandler({movieId: movieIdSchema}, 'params'), async (req, res, next)=> {
        cacheResponse(res, SIXTY_MINUTES_IN_SECONDS)
        const { movieId } = req.params

        try {
            const movies = await moviesService.getMovie({ movieId })

            res.status(200).json({
                data: movies,
                message: 'movies id'
            }) 
                

        } catch (error) {
            next(error)
        }
    })
    //POST
    router.post('/', validationHandler(createMovieSchema), async (req, res, next)=> {
        const { body: movie } = req

        try {
            const createMoviesId = await moviesService.createMove({ movie })

            res.status(201).json({
                data: createMoviesId,
                message: 'create movies'
            }) 
                

        } catch (error) {
            next(error)
        }
    })
    //PUT ID
    router.put('/:movieId',validationHandler({movieId: movieIdSchema}, 'params'), validationHandler(updateMovieShema), async (req, res, next)=> {
        const { movieId } = req.params
        const { body: movie } = req

        try {
            const updateMoveId = await moviesService.updateMove({ movieId, movie })

            res.status(200).json({
                data: updateMoveId,
                message: 'update movies '
            }) 
                

        } catch (error) {
            next(error)
        }
    })

        //PUT ID
        router.delete('/:movieId',validationHandler({movieId: movieIdSchema}, 'params'),  async (req, res, next)=> {
            const { movieId } = req.params

            try {
                const movieId = await moviesService.deleteMove({ movieId })
    
                res.status(200).json({
                    data: movieId,
                    message: 'delete movies '
                }) 
                    
    
            } catch (error) {
                next(error)
            }
        })
}



module.exports = {moviesApi}